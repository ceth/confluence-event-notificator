package de.cpotr.confluence.eventnotificator.listener;

/*   This file is part of confluence-event-notificator plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   confluence-event-notificator plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   confluence-event-notificator plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with confluence-event-notificator plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageMoveEvent;
import com.atlassian.confluence.event.events.content.page.PageRemoveEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.event.events.content.page.PageViewEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

/**
 * Annotated Event Listener for Page Events
 * 
 * @author Bastian Kraus <basti@cpotr.de>
 */
public class PageEventListener extends AbstractEventListener implements
		DisposableBean {
	private static final Logger log = LoggerFactory
			.getLogger(PageEventListener.class);

	protected EventPublisher eventPublisher;

	public PageEventListener(EventPublisher eventPublisher) {
		super(eventPublisher);
	}

	/**
	 * Event Listener Method for Page Create Events
	 * 
	 * @param event
	 *            {@link com.atlassian.confluence.event.events.content.page.PageCreateEvent}
	 */
	@EventListener
	public void pageCreateEvent(PageCreateEvent event) {
		log.debug("Page Create Event: '" + event.getPage().getTitle()
				+ "' in space '" + event.getPage().getSpaceKey() + "'");

	}

	/**
	 * Event Listener Method for Page Remove Events
	 * 
	 * @param event
	 *            {@link com.atlassian.confluence.event.events.content.page.PageRemoveEvent}
	 */
	@EventListener
	public void pageRemoveEvent(PageRemoveEvent event) {
		log.debug("Page Remove Event: '" + event.getPage().getTitle()
				+ "' in space '" + event.getPage().getSpaceKey() + "'");

	}

	/**
	 * Event Listener Method for Page Move Events
	 * 
	 * @param event
	 *            {@link com.atlassian.confluence.event.events.content.page.PageMoveEvent}
	 */
	@EventListener
	public void pageMoveEvent(PageMoveEvent event) {
		log.debug("Page Move Event: '" + event.getPage().getTitle()
				+ "' in space '" + event.getPage().getSpaceKey() + "'");

	}

	/**
	 * Event Listener Method for Page Update Events
	 * 
	 * @param event
	 *            {@link com.atlassian.confluence.event.events.content.page.PageUpdateEvent}
	 */
	@EventListener
	public void pageUpdateEvent(PageUpdateEvent event) {
		log.debug("Page Update Event: '" + event.getPage().getTitle()
				+ "' in space '" + event.getPage().getSpaceKey() + "'");

	}

	/**
	 * Event Listener Method for Page Remove Events
	 * 
	 * @param event
	 *            {@link com.atlassian.confluence.event.events.content.page.PageViewEvent}
	 */
	@EventListener
	public void pageViewEvent(PageViewEvent event) {
		log.debug("Page View Event: '" + event.getPage().getTitle()
				+ "' in space '" + event.getPage().getSpaceKey() + "'");

	}

}

package de.cpotr.confluence.eventnotificator.listener;

/*   This file is part of confluence-event-notificator plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   confluence-event-notificator plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   confluence-event-notificator plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with confluence-event-notificator plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LoginFailedEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.confluence.event.events.user.UserCreateEvent;
import com.atlassian.confluence.event.events.user.UserRemoveEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
 
/**
 * Annotated Event Listener for Page Events
 * 
 * @author Bastian Kraus <basti@cpotr.de>
 */
public class UserEventListener extends AbstractEventListener {
	
	private static final Logger log = LoggerFactory.getLogger(PageEventListener.class);
	
    public UserEventListener(EventPublisher eventPublisher) {	
    	super(eventPublisher);
	}

    /**
     * Event Listener Method for User Login Events
     * 
     * @param event {@link com.atlassian.confluence.event.events.security.LoginEvent}
     */
    @EventListener
    public void loginEvent(LoginEvent event) {
    	log.debug(String.format("User Login Event; username: %s; sessionid: %s; ip: %s", 
    			event.getUsername(),
    			event.getSessionId(),
    			event.getRemoteIP()));
    	
    	
    }

    /**
     * Event Listener Method for User Login Failed Events
     * 
     * @param event {@link com.atlassian.confluence.event.events.security.LoginFailedEvent}
     */
    @EventListener
    public void loginFailedEvent(LoginFailedEvent event) {
    	log.debug(String.format("User Login Failed Event; username: %s; sessionid: %s; ip: %s", 
    			event.getUsername(),
    			event.getSessionId(),
    			event.getRemoteIP()));
    	
    	
    }
 
    /**
     * Event Listener Method for User Logout Events
     * 
     * @param event {@link com.atlassian.confluence.event.events.security.LoginFailedEvent}
     */
    @EventListener
    public void logoutEvent(LogoutEvent event) {
    	log.debug(String.format("User Logout Event; username: %s; sessionid: %s; ip: %s", 
    			event.getUsername(),
    			event.getSessionId(),
    			event.getRemoteIP()));
    	
    	
    }

    /**
     * Event Listener Method for User Create Events
     * 
     * @param event {@link com.atlassian.confluence.event.events.user.UserCreateEvent}
     */
    @EventListener
    public void createEvent(UserCreateEvent event) {
    	log.debug("User Create Event; username: " + event.getUser());
    	
    	
    }

    /**
     * Event Listener Method for User Remove Events
     * 
     * @param event {@link com.atlassian.confluence.event.events.user.UserRemoveEvent}
     */
    @EventListener
    public void removeEvent(UserRemoveEvent event) {
    	log.debug("User Remove Event; username: " + event.getUser());
    	
    	
    }
    
}

package de.cpotr.confluence.eventnotificator.listener;

/*   This file is part of confluence-event-notificator plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   confluence-event-notificator plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   confluence-event-notificator plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with confluence-event-notificator plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import com.atlassian.event.api.EventPublisher;

/**
 * Abstract Event Listener<br/>
 * <br/>
 * Provides basic functionality for event listening.
 * 
 * @author Bastian Kraus <basti@cpotr.de>
 */
public abstract class AbstractEventListener implements DisposableBean {

	public static final Logger log = LoggerFactory.getLogger(AbstractEventListener.class);
	
    protected EventPublisher eventPublisher;

    public AbstractEventListener(EventPublisher eventPublisher) {
    	log.debug("Registering injected eventlistener...");
        this.eventPublisher = eventPublisher;
        eventPublisher.register(this);
    }
	

    /**
     * {@inheritDoc}
     */
	@Override
	public void destroy() throws Exception {
		log.debug("Unregistering eventlistener...");
		eventPublisher.unregister(this);
	}
	
}

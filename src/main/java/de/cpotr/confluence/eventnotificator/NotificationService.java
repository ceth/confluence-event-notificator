package de.cpotr.confluence.eventnotificator;

/*   This file is part of confluence-event-notificator plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   confluence-event-notificator plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   confluence-event-notificator plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with confluence-event-notificator plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface NotificationService {

	void pushNotificationForPageEvents();
	
}
